# wireguard_custodian

A systemd service that allows you to setup and manage you wireguard installation.
Also, you can give your friends access with an API key.

## Features

* [ ] run as root
* [ ] Package with APT
* [ ] setup wg-custodian interface
* [ ] Configuration
    * [x] Restore
    * [x] Save
    * [ ] Apply to Linux
* [ ] WUI for control
    * [x] Login with key
        * [x] Validate against DB
    * [ ] Admin
        * [ ] Add new IPs
        * [ ] Add Users
        * [ ] Remove Users
    * [ ] Users
        * [ ] Upload own key
        * [ ] Request IP
        * [ ] Request prefix `/64`

# WUI

* `/` The page for login and out

* `/admin` Overview
* `/admin/${id}` Modify User, Set key

* `/me` Users own page
* `/me/set-own-key`

* `/request/ip`
* `/request/prefix`

## Important documents

Template docs: https://tera.netlify.app/docs/
