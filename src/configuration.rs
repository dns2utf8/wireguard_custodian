use rand::Rng;
use serde::{Deserialize, Serialize};
use std::net::Ipv6Addr;
use std::path::Path;
use std::sync::{Arc, Mutex};

pub type Config = Arc<Mutex<ConfigRaw>>;
pub type WebConfig = actix_web::web::Data<crate::configuration::Config>;

const CONFIG_PATH: &str = "./wgc_config.json";

#[derive(Serialize, Deserialize)]
pub struct ConfigRaw {
    users: Vec<UserData>,
    // , serialize_with = "serialize_to_base64", deserialize_with = "base64::decode",
    #[serde(default)]
    cookie_private_key: Vec<u8>,
}

#[derive(Serialize, Deserialize)]
pub struct UserData {
    pub userkey: String,
    pub permissions: UserPermission,
    pub public_key: Option<String>,
    pub ips: Vec<Ipv6Addr>,
    pub prefixes: Vec<Ipv6Addr>,
}

#[derive(Serialize, Deserialize)]
pub enum UserPermission {
    IP,
    Prefix,
    Admin,
}

impl UserData {
    pub fn allow_prefix(&self) -> bool {
        match self.permissions {
            UserPermission::IP => false,
            UserPermission::Prefix | UserPermission::Admin => true,
        }
    }
    pub fn is_admin(&self) -> bool {
        match self.permissions {
            UserPermission::IP | UserPermission::Prefix => false,
            UserPermission::Admin => true,
        }
    }
}

pub fn restore_or_default() -> std::io::Result<ConfigRaw> {
    let path = Path::new(CONFIG_PATH);
    let mut save = false;
    let mut config: ConfigRaw;

    if path.exists() && path.is_file() {
        let data = std::fs::read_to_string(path)?;
        config = serde_json::from_str(&*data)?;

        info!("Restored configuration from {}", path.display());

        if config.cookie_private_key.len() < 32 {
            error!("cookie_private_key is to short! Regenerating");
            config.cookie_private_key = gen_private_key();
            save = true;
        }
    } else {
        info!("No configuration found, loading default");
        config = ConfigRaw {
            users: vec![],
            cookie_private_key: gen_private_key(),
        };
        save = true;
    }

    if config.users.iter().any(|u| u.is_admin()) == false {
        error!("No Admin Users found!");
        let userkey = config.add_user(UserPermission::Admin);
        save = true;
    }

    config
        .users
        .iter()
        .filter(|u| u.is_admin())
        .for_each(|u| info!("Admin User: {}", u.userkey));

    if save {
        config.save()?;
    }
    Ok(config)
}

impl ConfigRaw {
    pub fn save(&mut self) -> std::io::Result<()> {
        let data = serde_json::to_string_pretty(&self)?;

        std::fs::write(CONFIG_PATH, data)
    }

    pub fn get_user_data(&mut self, userkey: &str) -> Option<&mut UserData> {
        debug!("get_user_data({})", userkey);
        let userkey = userkey.trim();
        self.users
            .iter_mut()
            .filter(|ud| ud.userkey == userkey)
            .next()
    }

    pub fn get_private_key(&self) -> Vec<u8> {
        self.cookie_private_key.clone()
    }

    pub fn add_user(&mut self, permissions: UserPermission) -> String {
        let userkey = base64::encode(gen_private_key());

        self.users.push(UserData {
            userkey: userkey.clone(),
            permissions,
            public_key: None,
            ips: vec![],
            prefixes: vec![],
        });

        userkey
    }
}

fn gen_private_key() -> Vec<u8> {
    let mut private_key = vec![0u8; 32];

    // Generate a random 32 byte key. Note that it is important to use a unique
    // private key for every project. Anyone with access to the key can generate
    // authentication cookies for any user!
    rand::thread_rng().fill(&mut *private_key);

    private_key
}
