#[macro_use]
extern crate log;

use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use actix_identity::{CookieIdentityPolicy, Identity, IdentityService};
use actix_web::{error, middleware, web, App, Error, HttpResponse, HttpServer};
use serde::{Deserialize, Serialize};
use tera::Tera;

mod admin;
mod configuration;
mod me;
mod request;

// store tera template in application state
async fn index(
    id: Identity,
    tmpl: web::Data<tera::Tera>,
    query: web::Query<HashMap<String, String>>,
) -> Result<HttpResponse, Error> {
    let (mut ctx, _) = default_tera_context(&id);
    let s = if let Some(name) = query.get("name") {
        // submitted form
        ctx.insert("name", &name.to_owned());
        ctx.insert("text", &"Welcome!".to_owned());
        tmpl.render("me.html", &ctx)
            .map_err(|_| error::ErrorInternalServerError("Template error"))?
    } else {
        tmpl.render("index.html", &ctx)
            .map_err(|_| error::ErrorInternalServerError("Template error"))?
    };
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[derive(Serialize, Deserialize)]
pub struct UserContext {
    authenticated: bool,
    userkey: String,
}
pub fn new_user_context(id: &Identity) -> UserContext {
    let (authenticated, userkey) = match id.identity() {
        Some(userkey) => (true, userkey),
        None => (false, "Anonymous".to_owned()),
    };
    UserContext {
        authenticated,
        userkey,
    }
}

pub fn default_tera_context(id: &Identity) -> (tera::Context, UserContext) {
    let mut ctx = tera::Context::new();
    let user = new_user_context(&id);
    ctx.insert("user", &user);
    (ctx, user)
}

pub fn tera_user_users<'a>(
    id: &Identity,
    users: &'a configuration::WebConfig,
) -> (
    tera::Context,
    UserContext,
    std::sync::MutexGuard<'a, configuration::ConfigRaw>,
) {
    let (ctx, user) = default_tera_context(id);
    let users = users.lock().expect("unable to lock users");
    (ctx, user, users)
}

#[derive(Serialize, Deserialize)]
pub struct LoginParams {
    userkey: String,
}

async fn login(
    id: Identity,
    tmpl: web::Data<tera::Tera>,
    users: configuration::WebConfig,
    params: Option<web::Form<LoginParams>>,
    query: web::Query<HashMap<String, String>>,
) -> Result<HttpResponse, Error> {
    let next = match query.get("next") {
        Some(n) => n,
        None => "/",
    };

    let (mut ctx, _user) = default_tera_context(&id);

    if let Some(params) = params {
        let userkey = params.userkey.trim();
        let mut users = users.lock().expect("unable to lock users");

        if let Some(_user_data) = users.get_user_data(userkey) {
            id.remember(userkey.to_string());
            info!("login accepted: {:?}", id.identity());
            return Ok(HttpResponse::Found().header("location", next).finish());
        } else {
            ctx.insert("userkey", userkey);
        }
    }

    debug!("rejecte atempt");
    ctx.insert("next", next);

    let s = tmpl.render("sign_in_first.html", &ctx).map_err(|e| {
        warn!("me::login: {:?}", e);
        error::ErrorInternalServerError("Template error sign_in_first.html")
    })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

async fn logout(id: Identity) -> HttpResponse {
    id.forget();
    HttpResponse::Found().header("location", "/").finish()
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info,wireguard_custodian=debug");
    env_logger::init();

    let config = configuration::restore_or_default().expect("unable to restore configuration");
    let private_key = config.get_private_key();

    let config = Arc::new(Mutex::new(config));

    HttpServer::new(move || {
        let tera = Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*.html"))
            .expect("unable to load tera templates");

        App::new()
            .data(tera)
            .data(config.clone())
            .wrap(middleware::Logger::default()) // enable logger
            //.wrap(middleware::Compress::default())
            .wrap(IdentityService::new(
                // <- create identity middleware
                CookieIdentityPolicy::new(&private_key) // <- create cookie identity policy
                    .name("auth-cookie")
                    .secure(false),
                //.secure(true),
            ))
            .service(web::resource("/").route(web::get().to(index)))
            .service(web::resource("/me").route(web::get().to(me::index)))
            .service(web::resource("/me/login").to(login))
            .service(web::resource("/me/logout").to(logout))
            .service(web::resource("/me/set-own-key").route(web::get().to(me::set_own_key)))
            .service(web::resource("/me/set-own-key").route(web::post().to(me::apply_set_own_key)))
            .service(web::resource("/request/ip").to(request::ip))
            .service(web::resource("/request/prefix").to(request::prefix))
            .service(web::resource("/admin").to(admin::index))
            .service(
                // static files
                actix_files::Files::new("/static/", "./static/").index_file("index.html"),
            )
    })
    //.bind("127.0.0.1:8080")?
    .bind("[::]:8080")?
    .run()
    .await
}
