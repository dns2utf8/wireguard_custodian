use crate::{configuration::WebConfig, default_tera_context, new_user_context, tera_user_users};
use actix_identity::Identity;
use actix_web::{error, web, Error, HttpResponse};
use serde::{Deserialize, Serialize};

pub async fn index(
    id: Identity,
    users: WebConfig,
    tmpl: web::Data<tera::Tera>,
) -> Result<HttpResponse, Error> {
    let (mut ctx, user, mut users) = tera_user_users(&id, &users);
    debug!("id: {:?}", id.identity());

    let user_data = match users.get_user_data(&user.userkey) {
        None => {
            id.forget();
            return Ok(HttpResponse::Found()
                .header("location", "/me/login?next=/me")
                .finish());
        }
        Some(user_data) => user_data,
    };

    let ips = user_data
        .ips
        .iter()
        .map(|i| i.to_string())
        .collect::<Vec<String>>();
    ctx.insert("ips", &ips);

    let prefixes = vec!["bli".to_string()];
    ctx.insert("prefixes", &prefixes);

    let s = tmpl.render("me.html", &ctx).map_err(|e| {
        warn!("me::index: {:?}", e);
        error::ErrorInternalServerError("Template error me.html")
    })?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[derive(Serialize, Deserialize)]
pub struct SetKeyParams {
    userkey: String,
}
pub async fn set_own_key(
    id: Identity,
    users: WebConfig,
    tmpl: web::Data<tera::Tera>,
) -> Result<HttpResponse, Error> {
    let (mut ctx, user, mut users) = tera_user_users(&id, &users);

    let user_data = match users.get_user_data(&user.userkey) {
        None => {
            id.forget();
            return Ok(HttpResponse::Found()
                .header("location", "/me/login?next=/me/set-own-key")
                .finish());
        }
        Some(user_data) => user_data,
    };

    if let Some(public_key) = &user_data.public_key {
        ctx.insert("public_key", &public_key);
    }

    let s = tmpl.render("set_own_key.html", &ctx).map_err(|e| {
        warn!("me::set_own_key: {:?}", e);
        error::ErrorInternalServerError("Template error set_own_key.html")
    })?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
pub async fn apply_set_own_key(
    id: Identity,
    users: WebConfig,
    params: web::Form<SetKeyParams>,
) -> HttpResponse {
    let user = new_user_context(&id);
    if user.authenticated == false {
        return HttpResponse::Found()
            .header("location", "/me/login?next=/me/set-own-key")
            .finish();
    }

    let s = format!("TODO");
    HttpResponse::Ok().content_type("text/html").body(s)
}
