use crate::{configuration::WebConfig, default_tera_context, tera_user_users};
use actix_identity::Identity;
use actix_web::{error, web, Error, HttpResponse};
use serde::{Deserialize, Serialize};
use std::net::Ipv6Addr;

#[derive(Serialize, Deserialize)]
pub struct AddressParams {
    address: String,
}

pub async fn ip(
    id: Identity,
    users: WebConfig,
    tmpl: web::Data<tera::Tera>,
    params: Option<web::Form<AddressParams>>,
) -> Result<HttpResponse, Error> {
    let (mut ctx, user, mut users) = tera_user_users(&id, &users);

    let user_data = match users.get_user_data(&user.userkey) {
        None => {
            id.forget();
            return Ok(HttpResponse::Found()
                .header("location", "/me/login?next=/request/ip")
                .finish());
        }
        Some(user_data) => user_data,
    };

    ctx.insert("type", "IPv6");
    ctx.insert("target", "ip");

    let mut messages = vec![];

    if let Some(params) = params {
        let address = params.address.parse::<Ipv6Addr>();
        messages.push(format!("{:?}", address));

        ctx.insert("address", &params.address)
    }

    ctx.insert("messages", &messages);

    let s = tmpl.render("request.html", &ctx).map_err(|e| {
        warn!("me::index: {:?}", e);
        error::ErrorInternalServerError("Template error request.html")
    })?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

pub async fn prefix(
    id: Identity,
    users: WebConfig,
    tmpl: web::Data<tera::Tera>,
    params: Option<web::Form<AddressParams>>,
) -> Result<HttpResponse, Error> {
    let (mut ctx, user) = default_tera_context(&id);

    if user.authenticated == false {
        return Ok(HttpResponse::Found()
            .header("location", "/me/login?next=/request/prefix")
            .finish());
    }

    ctx.insert("type", "IPv6 Prefix");
    ctx.insert("target", "prefix");

    let mut messages = vec![];

    if let Some(params) = params {
        let address = params.address.parse::<Ipv6Addr>();
        messages.push(format!("{:?}", address));

        ctx.insert("address", &params.address)
    }

    ctx.insert("messages", &messages);

    let s = tmpl.render("request.html", &ctx).map_err(|e| {
        warn!("me::index: {:?}", e);
        error::ErrorInternalServerError("Template error request.html")
    })?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
