use crate::{configuration::WebConfig, default_tera_context};
use actix_identity::Identity;
use actix_web::{error, web, Error, HttpResponse};
use serde::{Deserialize, Serialize};

pub async fn index(
    id: Identity,
    tmpl: web::Data<tera::Tera>,
    users: WebConfig,
    params: web::Form<Option<AdminParams>>,
) -> Result<HttpResponse, Error> {
    let (mut ctx, user) = default_tera_context(&id);
    let template_name;

    if user.authenticated {
        template_name = "me.html";

        let ips = vec!["bla".to_string()];
        ctx.insert("ips", &ips);

        let prefixes = vec!["bli".to_string()];
        ctx.insert("prefixes", &prefixes);
    } else {
        ctx.insert("next", "/me");
        template_name = "sign_in_first.html";
    }
    let s = tmpl.render(template_name, &ctx).map_err(|e| {
        warn!("me::index: {:?}", e);
        error::ErrorInternalServerError("Template error me.html")
    })?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[derive(Serialize, Deserialize)]
pub struct AdminParams {
    action: String,
    userkey: String,
}
