//#![allow(unused)]
//! This tool will fetch its configuration from a main/central wgc and distribute
//! this configuration to the local network
//!
//! Steps:
//! * sudo
//! * ping uplink via wireguard VPN
//! * enable routing
//! * send RA

use pnet::datalink::Channel::Ethernet;
use pnet::datalink::{self, MacAddr, NetworkInterface};
use pnet::ipnetwork::{IpNetwork, Ipv6Network};
use pnet::packet::{
    ethernet::{EtherTypes, MutableEthernetPacket},
    icmpv6::{
        ndp::{Icmpv6Codes, MutableRouterAdvertPacket, NdpOption, NdpOptionTypes, RouterAdvert},
        Icmpv6Types,
    },
    ip::IpNextHeaderProtocols,
    ipv6::MutableIpv6Packet,
    MutablePacket, Packet,
};
use pnet::util;
use std::error::Error;
use std::thread::sleep;
use std::time::{Duration, Instant};

struct PrefixConfig {
    prefix: Ipv6Network,
    valid_lifetime_secs: u32,
    preferred_lifetime_secs: u32,
}

fn main() -> Result<(), Box<dyn Error>> {
    // `sudo setcap cap_net_raw+ep /path/to/exec` or run program with sudo. May not work on Os X.
    sudo::escalate_if_needed().unwrap();

    // TODO ping gw over wireguard

    // TODO enable routing on local system

    send_ra()
}

fn send_ra() -> Result<(), Box<dyn Error>> {
    let interface_name = "enp4s0";
    let beacon_time = Duration::from_secs(10);

    for interface in datalink::interfaces()
        .into_iter()
        .filter(|e| e.is_up() && !e.is_loopback() && e.ips.len() > 0)
    {
        let name = &interface.name;
        if name.starts_with("veth") || name.contains("docker") {
            continue;
        }
        println!("{}", interface);
    }

    // Find the network interface with the provided name
    let interfaces = datalink::interfaces();
    let interface = interfaces
        .into_iter()
        .filter(|e| e.is_up() && !e.is_loopback() && e.ips.len() > 0)
        .filter(|iface: &NetworkInterface| iface.name == interface_name)
        .next()
        .expect(&format!("unable to find interface {:?}", interface_name));

    let prefix_config = PrefixConfig {
        prefix: "2a0a:e5c1:177::/64".parse().unwrap(),
        preferred_lifetime_secs: 30,
        valid_lifetime_secs: 60,
    };
    let fe80: Ipv6Network = "fe80::/64".parse().unwrap();
    let interface_fe80_ip = interface
        .ips
        .iter()
        .filter_map(|i| match i {
            IpNetwork::V4(_) => None,
            IpNetwork::V6(n) => {
                if n.is_subnet_of(fe80) {
                    Some(n.ip())
                } else {
                    None
                }
            }
        })
        .next()
        .expect("interface does not contain a fe80:: address");
    println!(
        "Selected interface \"{}\" with {:?}",
        interface_name, interface_fe80_ip
    );

    // Create a new channel, dealing with layer 2 packets
    let mut config: datalink::Config = Default::default();
    config.read_timeout = Some(Duration::from_millis(128));
    let (mut tx, mut rx) = match datalink::channel(&interface, config) {
        Ok(Ethernet(tx, rx)) => (tx, rx),
        Ok(_) => panic!("Unhandled channel type"),
        Err(e) => panic!(
            "An error occurred when creating the datalink channel: {}",
            e
        ),
    };

    // rx <= icmp_packet_iter ???

    let advert_flags = 0; // if DHCPv6 has an IP: RouterAdvertFlags::ManagedAddressConf
    let announcement_lifetime = prefix_config.preferred_lifetime_secs; // seconds
    assert!(
        prefix_config.preferred_lifetime_secs <= prefix_config.valid_lifetime_secs,
        "preferred must not exceed valid lifetime"
    );
    let ff02_1 = "ff02::1".parse().unwrap();
    let num_packets_per_round = 1;

    let mut packet_size = 0;
    // ##################################################### BEGIN advert ###########################################

    let (advert, size_extensions) = {
        let m = interface.mac.unwrap();
        let options = vec![
            NdpOption {
                option_type: NdpOptionTypes::PrefixInformation,
                length: 4,
                data: build_prefix_information(&prefix_config),
            },
            /*NdpOption {
                option_type: NdpOptionTypes::MTU,
                length: 1,
                data: vec![0, 0, 0x00, 0x00, 0x05, 0xdc], // first two are reserved: 1500_u32.bytes(),
            },*/
            NdpOption {
                option_type: NdpOptionTypes::SourceLLAddr,
                length: 1,
                data: vec![m.0, m.1, m.2, m.3, m.4, m.5],
            },
        ];
        let size_extensions: usize = options.iter().map(|o| o.length as usize).sum();
        (
            RouterAdvert {
                icmpv6_type: Icmpv6Types::RouterAdvert,
                icmpv6_code: Icmpv6Codes::NoCode,
                checksum: 0,
                hop_limit: 0x01,
                flags: advert_flags,
                lifetime: announcement_lifetime as u16, // seconds
                reachable_time: 0,                      // ms
                retrans_time: 0,                        // ms
                options,
                payload: vec![],
            },
            size_extensions,
        )
    };

    //packet_size += MutableRouterAdvertPacket::packet_size(&advert) - 3  + size_extensions * 8;
    packet_size += 16 + size_extensions * 8;
    let mut advert_buffer = vec![0u8; packet_size];
    println!(
        "  reserving {} Bytes for MutableRouterAdvertPacket",
        advert_buffer.len()
    );
    let mut advert_package =
        MutableRouterAdvertPacket::new(&mut advert_buffer).expect("ndp::AdvertPacket");
    advert_package.populate(&advert);
    /*let checksum = util::checksum(&advert_package.packet_mut(), 1);
    advert_package.set_checksum(checksum);*/
    //println!("{:?}", advert_package);
    // ##################################################### BEGIN icmpv6 ###########################################

    /*
    packet_size += MutableIcmpv6Packet::minimum_packet_size();
    let mut icmpv6_buffer = vec![0u8; packet_size];
    println!(
        "  reserving {} Bytes for MutableIcmpv6Packet",
        icmpv6_buffer.len()
    );
    let mut ip6_packet = MutableIcmpv6Packet::new(&mut ip6_buffer).unwrap();

    icmpv6_buffer.set_icmpv6_type(Icmpv6Types::RouterAdvert);
    icmpv6_buffer.set_source(interface_fe80_ip);
    icmpv6_buffer.set_destination(ff02_1);
    icmpv6_buffer.set_next_header(IpNextHeaderProtocols::Icmpv6);

    // ##################################################### BEGIN ip6 ########################################### */
    let icmp_size = packet_size;
    packet_size += MutableIpv6Packet::minimum_packet_size();
    let mut ip6_buffer = vec![0u8; packet_size];
    println!(
        "  reserving {} Bytes for MutableIpv6Packet",
        ip6_buffer.len()
    );
    let mut ip6_packet = MutableIpv6Packet::new(&mut ip6_buffer).unwrap();

    ip6_packet.set_version(6);
    ip6_packet.set_source(interface_fe80_ip);
    ip6_packet.set_destination(ff02_1);
    ip6_packet.set_next_header(IpNextHeaderProtocols::Icmpv6);
    // update icmpv6 checksum
    advert_package.set_checksum(checksum(&advert_package, &interface_fe80_ip, &ff02_1));
    //advert_package.set_checksum(checksum(&advert_package.packet()));
    // If this is not set some assertion will fail later
    ip6_packet.set_payload_length(icmp_size as u16);
    // ##################################################### BEGIN Ethernet ###########################################

    packet_size += MutableEthernetPacket::minimum_packet_size();
    let mut ethernet_buffer = vec![0u8; packet_size];
    println!(
        "  reserving {} Bytes for MutableEthernetPacket",
        ethernet_buffer.len()
    );
    let mut ethernet_packet = MutableEthernetPacket::new(&mut ethernet_buffer).unwrap();

    ethernet_packet.set_source(interface.mac.unwrap());
    ethernet_packet.set_destination(MacAddr::broadcast());
    ethernet_packet.set_ethertype(EtherTypes::Ipv6);
    // ##################################################### BEGIN Schachteln ###########################################

    flush_stdout()?;
    ip6_packet.set_payload(&advert_package.packet_mut());
    ethernet_packet.set_payload(&ip6_packet.packet_mut());

    ////////////////// DEBUG PRINT ////////////////////////
    let print_buffer = |name, buf: &[u8]| {
        println!("\n##### {} ####", name);
        for (i, x) in buf.iter().enumerate() {
            if i % 32 == 0 {
                print!("\n");
            }
            print!("{:2x}, ", x);
        }
        println!("");
    };
    print_buffer("advert_package", advert_package.packet());
    print_buffer("ip6_packet", ip6_packet.packet());
    print_buffer("ethernet_packet", ethernet_packet.packet());
    ////////////////// DEBUG PRINT ////////////////////////

    //let packet_size = ethernet_packet.packet().len();
    if packet_size == 0 {
        panic!("messed up with the syncinc");
    }

    loop {
        let loop_start_time = Instant::now();
        // handle potentially incomin traffic, don't care
        let _ = rx.next();

        tx.build_and_send(num_packets_per_round, packet_size, &mut |new_packet| {
            let mut new_packet = MutableEthernetPacket::new(new_packet).unwrap();

            // Create a clone of the original packet
            new_packet.clone_from(&ethernet_packet);

            // Switch the source and destination
            //new_packet.set_source(packet.get_destination());
            //new_packet.set_destination(packet.get_source());
        })
        .expect("not enough memory to build package")
        .expect("build_and_send error");

        let elapsed = loop_start_time.elapsed();
        println!("finished round in {:?}", elapsed);
        if elapsed < beacon_time {
            sleep(beacon_time - elapsed);
        }

        //return Ok(());
    }

    //Ok(())
}

fn checksum(
    packet: &MutableRouterAdvertPacket,
    source: &std::net::Ipv6Addr,
    destination: &std::net::Ipv6Addr,
) -> u16 /* u16be */ {
    //use util;
    //use Packet;

    util::ipv6_checksum(
        packet.packet(),
        1,
        &[],
        source,
        destination,
        IpNextHeaderProtocols::Icmpv6,
    )
}

fn build_prefix_information(config: &PrefixConfig) -> Vec<u8> {
    let mut v = Vec::with_capacity(32);

    let (prefix_lenght, subnet) = (config.prefix.prefix(), config.prefix.ip());

    v.push(prefix_lenght);

    v.push(
        (1 << 7) // on-link flag
        + (1 << 6), // stateless address flag
    );

    // valid lifetime
    v.extend_from_slice(&config.valid_lifetime_secs.to_be_bytes());
    // preferred lifetime
    v.extend_from_slice(&config.preferred_lifetime_secs.to_be_bytes());
    // reserved 2
    v.extend_from_slice(&[0, 0, 0, 0]);

    v.extend_from_slice(&subnet.octets());

    v
}

fn flush_stdout() -> std::io::Result<()> {
    use std::io::Write;
    std::io::stdout().flush()?;
    std::io::stderr().flush()
}
